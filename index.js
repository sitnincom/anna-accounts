"use strict";

const Promise = require("bluebird");
const async = require('asyncawait/async');
const await = require('asyncawait/await');
const bodyParser = require('body-parser')
const bcrypt = require('bcrypt');
const uuid = require("uuid");
const AWebappModule = require("anna-express/AWebappModule");

module.exports = class AccountsModule extends AWebappModule {
    constructor (core, settings) {
        super(core, settings);

        this.requiredModules.push("anna-session");
        this.requiredModules.push("anna-postgres");
        this.requiredModules.push("anna-emailer");

        const urlencoded = bodyParser.urlencoded({ extended: false });

        this._urls.add(["post", "/auth/reg/submit", this.registerSubmit, [urlencoded]]);
        this._urls.add(["get", "/auth/reg/verify", this.verifyAccount]);
        this._urls.add(["get", "/auth/reg/", this.registerPage]);

        this._urls.add(["post", "/auth/recover/submit", this.recoverSubmit, [urlencoded]]);
        this._urls.add(["get", "/auth/recover/apply", this.recoverApply]);
        this._urls.add(["get", "/auth/recover/", this.recoverPage]);

        this._urls.add(["post", "/auth/login/submit", this.loginSubmit, [urlencoded]]);
        this._urls.add(["get", "/auth/login/", this.loginPage]);

        this._urls.add(["post", "/auth/setNewPassword", this.setNewPassword, [urlencoded]]);

        this._urls.add(["get", "/auth/logout", this.logout]);
    }

    get mailer () {
        return this.modules.get("anna-emailer");
    }

    postInit () {
        super.postInit();
        this.registerModels(require("./models.js"));
    }

    logout (req, res) {
        req.session.user = null;
        res.redirect(req.query.next || "/");
    }

    loginPage (req, res) {
        res.render("auth/login.html", {
            nextUrl: req.query.next || "/"
        });
    }
    
    bcryptCompare (sample, hash) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(sample, hash, (err, res) => {
                if (err) {
                    reject(err);
                } else if (!res) {
                    reject(new Error("Passwords doesn't match"));
                } else {
                    resolve(true);
                }
            });
        });
    }

    bcryptHash (original, rounds) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(original, rounds, (err, res) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(res);
                }
            });
        });
    }

    loginSubmit (req, res) {
        const Account = this.getModel("Account");
        const Profile = this.getModel("Profile");

        const email = req.body.email; 
        const password = req.body.password; 

        async(_ => {
            const account = await(Account.findActiveByEmail(email));
            if (!!account) {
                const hashTest = await(this.bcryptCompare(password, account.passhash));
                if (hashTest) {
                    const profile = await(Profile.findByAccountId(account.id));
                    return [account, profile];
                } 
            };
            return null;
        })().then(results => {
            if (!!results && true === results[0].is_active) {
                req.session.user = {
                    account: results[0],
                    profile: results[1]
                };
                res.redirect(req.query.next || "/");
            } else {
                res.render("error.html", {
                    title: "Пользователь не найден",
                    msg: "Возможно, вы ошиблись в емейле или пароле. Попробуйте вернуться и ввести их снова."
                });
            }
        }).catch(err => {
            res.render("error.html", { title: "Проблема при авторизации пользователя", msg: err.message, stack: err.stack });
        });
    }

    registerPage (req, res) {
        res.render("auth/register.html", {
            nextUrl: req.query.next || "/"
        });
    }

    recoverPage (req, res) {
        res.render("auth/recover.html", {
            nextUrl: req.query.next || "/"
        });
    }
    
    generateNewPassword (length) {
        let alphabet = "123456789abcdefghijkmnpqrstwxyzABCDEFGHIJKLMNPQRSTWXYZ";

        let password = "";
        while (password.length < length) {
            const char = alphabet[this.core.randomInt(0, alphabet.length)];
            if (-1 !== alphabet.indexOf(char)) {
                password += char;
            }
        }

        return password;
    }

    registerSubmit (req, res) {
        const Account = this.getModel("Account");
        const Profile = this.getModel("Profile");

        const email = req.body.email; 
        const name = req.body.name; 
        const lastname = req.body.lastname; 
        const nextUrl = req.query.next || "/";

        async(_ => {
            const test_account = await(Account.findByEmail(email));
            if (!!test_account) {
                return null;
            }

            const account_id = await(Account.createWithRole(email, null, "user", false))[0].id;
            this.log.debug(`Created new account with ID ${account_id}`);

            const profile_id = await(Profile.createWithAccountId(account_id, name, lastname, `${name} ${lastname}`))[0].id;
            this.log.debug(`Created new profile ${profile_id} for account ${account_id}`);

            const recovery_code = uuid.v1();
            const recovery = await(Account.setRecoveryCodeById(account_id, recovery_code));
            
            const verificationEmail = await(this.mailer.sendEmail("auth/verification.html", {
                domain: this.core._config.get("global.domain"),
                email: email,
                code: recovery_code
            }, {
                subject: "Подтверждение регистрации",
                to: email
            }));

            return account_id;
        })().then(results => {
            if (!!results) {
                res.render("auth/accountCreated.html", {
                    email: email
                });
            } else {
                res.render("auth/emailAlreadyClaimed.html", {
                    email: email,
                    nextUrl: nextUrl
                });
            }
        }).catch(err => {
            res.render("error.html", { title: "Проблема при регистрации", msg: err.message, stack: err.stack });
        });
    }

    recoverSubmit (req, res) {
        const Account = this.getModel("Account");
        const Profile = this.getModel("Profile");

        const email = req.body.email; 
        const nextUrl = req.query.next || "/";

        async(_ => {
            const account = await(Account.findActiveByEmail(email));
            if (!account) {
                this.log.debug(`Account with email ${email} not found`);
                return null;
            } else {
                const recovery_code = uuid.v1();
                const recovery = await(Account.setRecoveryCodeById(account.id, recovery_code));

                const recoverEmail = await(this.mailer.sendEmail("auth/recover.html", {
                    siteTitle: this.core._config.get("global.title"),
                    domain: this.core._config.get("global.domain"),
                    email: email,
                    code: recovery_code
                }, {
                    subject: "Восстановление пароля к сайту" + this.core._config.get("global.title"),
                    to: email
                }));

                return account;
            }
        })().then(account => {
            res.render("auth/recoverCodeSent.html", {
                email: email
            });

            return account;
        }).then(account => {
            if (!!account) {
                this.core.emit("account.registered", account.id);
            }
        }).catch(err => {
            res.render("error.html", { title: "Проблема при восстанолении пароля", msg: err.message, stack: err.stack });
        });
    }

    recoverApply (req, res) {
        const Account = this.getModel("Account");
        const Profile = this.getModel("Profile");

        const email = req.query.email; 
        const code = req.query.code || "undefined code"; 
        const nextUrl = req.query.next || "/";

        async(_ => {
            const account = await(Account.findActiveByEmail(email));
            if (!account) {
                this.log.debug(`Account with email ${email} not found`);
                return null;
            }

            if (account.recovery_code !== code) {
                this.log.debug(`Invalid recovery code ${code} for account with email ${email}`);
                this.log.debug(`Correct code is ${account.recovery_code}`);
                return null;
            }

            const droppingCode = await(Account.setRecoveryCodeById(account.id, null));
            account.recovery_code = null;

            const password = this.generateNewPassword(10);
            const passhash = await(this.bcryptHash(password, 12)); // 12 rounds
            const settingPassword = await(Account.setPassword(account.id, passhash));
            account.passhash = passhash;

            const profile = await(Profile.findByAccountId(account.id));

            const registeredEmail = await(this.mailer.sendEmail("auth/new_password.html", {
                siteTitle: this.core._config.get("global.title"),
                domain: this.core._config.get("global.domain"),
                account: account,
                profile: profile,
                password: password
            }, {
                subject: "Ваш новый пароль для доступа к сайту " + this.core._config.get("global.title"),
                to: email
            }));

            return [account, profile];
        })().then(results => {
            if (!!results) {
                req.session.user = {
                    account: results[0],
                    profile: results[1]
                };
                
                res.render("auth/passwordChanged.html", {
                    account: results[0],
                    profile: results[1],
                    next: nextUrl
                });
            } else {
                throw new Error(`Невозможно усановить новый пароль учётной записи`);
            }
        }).catch(err => {
            res.render("error.html", { title: "Проблема при восстанолении пароля", msg: err.message, stack: err.stack });
        });
    }

    setNewPassword (req, res) {
        const Account = this.getModel("Account");
        const Profile = this.getModel("Profile");
        
        const account_id = req.body.account_id || null;
        const password = req.body.password; 
        const new_password = req.body.new_password; 
        const nextUrl = req.query.next || "/";

        async(_ => {
            const account = await(Account.findById(account_id));
            if (!account) {
                this.log.error(`Account with id ${account_id} not found`);
                return null;
            }

            const hashTest = await(this.bcryptCompare(password, account.passhash));
            if (!hashTest) {
                return null;
            } 
            
            const passhash = await(this.bcryptHash(new_password, 12)); // 12 rounds
            const settingPassword = await(Account.setPassword(account.id, passhash));
            account.passhash = passhash;

            const profile = await(Profile.findByAccountId(account.id));

            const email = await(this.mailer.sendEmail("auth/new_password.html", {
                siteTitle: this.core._config.get("global.title"),
                domain: this.core._config.get("global.domain"),
                account: account,
                profile: profile,
                password: new_password
            }, {
                subject: "Ваш новый пароль для доступа к сайту " + this.core._config.get("global.title"),
                to: account.login
            }));

            return [account, profile];
        })().then(results => {
            if (!!results) {
                res.render("auth/passwordChanged.html", {
                    account: results[0],
                    profile: results[1],
                    next: nextUrl
                });
            } else {
                throw new Error(`Невозможно изменить пароль учётной записи`);
            }
        }).catch(err => {
            res.render("error.html", { title: "Проблема при изменении пароля", msg: err.message, stack: err.stack });
        });
    }

    verifyAccount (req, res) {
        const Account = this.getModel("Account");
        const Profile = this.getModel("Profile");

        const email = req.query.email; 
        const code = req.query.code || "undefined code"; 
        const nextUrl = req.query.next || "/";

        async(_ => {
            const account = await(Account.findByEmail(email));

            if (!account || account.recovery_code !== code) {
                return null;
            }
            
            const droppingCode = await(Account.setRecoveryCodeById(account.id, null));
            account.recovery_code = null;

            const password = this.generateNewPassword(10);
            const passhash = await(this.bcryptHash(password, 12)); // 12 rounds
            const settingPassword = await(Account.setPassword(account.id, passhash));
            account.passhash = passhash;

            const activatingAccount = await(Account.setActive(account.id, true));
            account.active = null;

            const profile = await(Profile.findByAccountId(account.id));

            const registeredEmail = await(this.mailer.sendEmail("auth/registered.html", {
                domain: this.core._config.get("global.domain"),
                account: account,
                profile: profile,
                password: password
            }, {
                subject: "Ваш пароль для доступа к сайту " + this.core._config.get("global.title"),
                to: email
            }));

            return [account, profile];
        })().then(results => {
            if (!!results) {
                req.session.user = {
                    account: results[0],
                    profile: results[1]
                };
                
                res.render("auth/accountVerified.html", {
                    account: results[0],
                    profile: results[1],
                    next: nextUrl
                });
            } else {
                throw new Error(`Невозможно подтвердить учётную запись`);
            }
        }).then(_ => {
            this.core.emit("account.verified", req.session.user.account.id);
        }).catch(err => {
            res.render("error.html", { title: "Проблема при регистрации", msg: err.message, stack: err.stack });
        });
    }

    updateLocals (req, res) {
        this.log.debug(`${this.constructor.name}.updateLocals()`);

        if (!!req.session && !!req.session.user) {
            res.locals.user = req.session.user;
            res.locals.isGuest = false;
            res.locals.isUser = true;
            res.locals.isAdmin = "admin" === req.session.user.role;
        } else {
            res.locals.user = null;
            res.locals.isGuest = true;
            res.locals.isUser = false;
            res.locals.isAdmin = false;
        }
    }

    init () {
        super.init();

        this.injectLocals(this.updateLocals.bind(this));

        return Promise.resolve();
    }
}
