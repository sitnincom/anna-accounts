"use strict";

const path = require("path");
const async = require('asyncawait/async');
const await = require('asyncawait/await');
const ADbModel = require("anna-postgres/ADbModel");

class AccountModel extends ADbModel {
    get table () {
        return "accounts";
    }

    sql (filename) {
        const realFilename = path.resolve(path.join(__dirname, filename));
        return super.sql(realFilename);
    }

    findByEmail (email) {
        const query = `SELECT * FROM ${this.fullTableName} WHERE login=$1`;
        return this.db.oneOrNone(query, [email]);
    }

    findActiveByEmail (email) {
        const query = `SELECT * FROM ${this.fullTableName} WHERE login=$1 AND is_active='t'`;
        return this.db.oneOrNone(query, [email]);
    }
    
    createWithRole (login, passhash, role, active) {
        return this.db.query(this.sql("./sql/createAccount.sql"), {
            login: login,
            passhash: passhash,
            role: role,
            active: !!active
        });
    }
    
    setRecoveryCodeById (id, code) {
        return this.db.query(this.sql("./sql/setRecoveryCode.sql"), {
            id: id,
            code: code
        });
    }

    setActive (id, active) {
        return this.db.query(this.sql("./sql/setAccountActive.sql"), {
            id: id,
            active: active
        });
    }

    setPassword (id, password) {
        return this.db.query(this.sql("./sql/setAccountPassword.sql"), {
            id: id,
            password: password
        });
    }
}

class ProfileModel extends ADbModel {
    get table () {
        return "profiles";
    }

    sql (filename) {
        const realFilename = path.resolve(path.join(__dirname, filename));
        return super.sql(realFilename);
    }

    findByAccountId (email) {
        const query = `SELECT * FROM ${this.fullTableName} WHERE account_id=$1`;
        return this.db.oneOrNone(query, [email]);
    }

    update (id, name, lastname, nickname) {
        return this.db.query(this.sql("./sql/updateProfile.sql"), {
            id: id,
            name: name,
            lastname: lastname,
            nickname: nickname
        });
    }

    setAvatar (id, avatar) {
        return this.db.query("UPDATE public.profiles SET avatar=${avatar} WHERE id=${id}", {
            id: id,
            avatar: avatar
        });
    }

    createWithAccountId (account_id, name, lastname, nickname, extra) {
        return this.db.query(this.sql("./sql/createProfile.sql"), {
            account: account_id,
            name: name,
            lastname: lastname,
            nickname: nickname,
            extra: extra
        });
    }
    
    setExtra (id, extra) {
        return this.db.query(this.sql("./sql/setProfileExtra.sql"), {
            id: id,
            extra: extra
        });
    }
    
    updateExtra (id, update) {
        return async(_ => {
            const profile = await(this.findById(id));
            if (!profile) {
                throw new Exception(`Cannot load profile ${id} data`);
            };

            const new_extra = {};
            Object.assign(new_extra, profile.extra, update);

            return await(this.setExtra(id, new_extra));
        })();
    }
}

module.exports = {
    Account: AccountModel,
    Profile: ProfileModel
}
