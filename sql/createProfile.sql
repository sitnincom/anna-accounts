INSERT INTO public.profiles (account_id, name, lastname, nickname, extra)
VALUES (${account}, ${name}, ${lastname}, ${nickname}, ${extra})
RETURNING id;
